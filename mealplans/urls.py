from django.urls import path

from mealplans.views import (
    MealPlanListView,
    MealPlanCreateView,
    MealPlanView,
    MealPlanUpdateView,
    MealPlanDeleteView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplan_list"),
    path("new/", MealPlanCreateView.as_view(), name="mealplan_new"),
    path("<int:pk>/", MealPlanView.as_view(), name="mealplan_detail"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="mealplan_edit"),
    path("<int:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplan_delete"),
]

# urlpatterns = [
#     path("", RecipeListView.as_view(), name="recipes_list"),
#     path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
#     path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
#     path("new/", RecipeCreateView.as_view(), name="recipe_new"),
#     path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
#     path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),

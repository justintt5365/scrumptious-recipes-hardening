from django.contrib import admin
from django.apps import AppConfig
from .models import MealPlan


class MealPlansConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "mealplans"


admin.site.register(MealPlan)
